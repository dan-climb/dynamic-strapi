import dynamic from "next/dynamic";

const Hero = dynamic(() => import("./hero"));
const Tagline = dynamic(() => import("./tagline"));
const ImageTagline = dynamic(() => import("./image-tagline"));

type DynamicProps = {
  components: {
    __component: string;
    [key: string]: any;
  }[];
};

// List should map to components in CMS
const componentList = {
  global: {
    hero: Hero,
    tagline: Tagline,
    "image-tagline": ImageTagline,
  },
};

export default function Dynamic({ components }: DynamicProps) {
  return (
    <div>
      {components.map((c) => {
        const Component = resolve(c.__component, componentList);
        if (!Component) return null;
        return <Component {...c} />;
      })}
    </div>
  );
}

const resolve = (selector: string, obj: any) =>
  selector.split(".").reduce((acc, cur) => (acc ? acc[cur] : null), obj);
