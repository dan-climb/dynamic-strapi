import { GetStaticPropsContext } from "next";
import Dynamic from "../components/dynamic";
import { getPageBySlug, getSlugs, Page as TPage } from "../utils";

export default function Page({ page }: { page: TPage }) {
  return <Dynamic components={page.Components} />;
}

export async function getStaticPaths() {
  const paths = await getSlugs();
  return {
    paths,
    fallback: false,
  };
}

export async function getStaticProps({ params }: GetStaticPropsContext) {
  const slug =
    typeof params?.slug === "string" ? params.slug : params?.slug?.join("-");

  if (!slug) return null;

  const [page] = await getPageBySlug(slug);

  return {
    props: {
      page,
    },
  };
}
