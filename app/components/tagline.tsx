import styled from "styled-components";

type TaglineProps = {
  Title: string;
  Description: string;
};

export default function Tagline({ Title, Description }: TaglineProps) {
  return (
    <Wrapper>
      <h1>{Title}</h1>
      <p>{Description}</p>
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  padding: 2rem 0;
`;
