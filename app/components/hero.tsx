import styled from "styled-components";
import { cmsUrl } from "../utils";

type HeroProps = {
  Title: string;
  Description: string;
  Image: {
    width: number;
    height: number;
    url: string;
  };
};

export default function Hero({ Title, Description, Image }: HeroProps) {
  return (
    <Wrapper url={Image.url}>
      <h1>{Title}</h1>
      <p>{Description}</p>
    </Wrapper>
  );
}

const Wrapper = styled.div<{ url: string }>`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  height: 45vh;
  background-image: ${(p) => `url(${cmsUrl}${p.url})`};
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
  color: white;
`;
