import styled from "styled-components";
import Image from "next/image";
import { cmsUrl } from "../utils";

type ImageTaglineProps = {
  Title: string;
  Description: string;
  Image: {
    width: number;
    height: number;
    url: string;
  };
};

export default function ImageTagline({
  Title,
  Description,
  Image: image,
}: ImageTaglineProps) {
  return (
    <Wrapper>
      <Content>
        <h1>{Title}</h1>
        <p>{Description}</p>
      </Content>
      <Image {...image} src={`${cmsUrl}${image.url}`} />
    </Wrapper>
  );
}

const Wrapper = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
`;

const Content = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
  justify-content: center;
  padding-right: 2rem;
`;
