import axios from "axios";

export const cmsUrl =
  process.env.NEXT_PUBLIC_CMS_API_URL || "http://localhost:1337";

const cms = axios.create({
  baseURL: cmsUrl,
});

export type Page = {
  id: number;
  Path: string;
  Slug: string;
  Title: string;
  Components: {
    __component: "string";
    [key: string]: any;
  }[];
};

export const getAllPages = async () => {
  const { data } = await cms.get<Page[]>(`/templates`);
  return data;
};

export const getPageBySlug = async (slug: string) => {
  const { data } = await cms.get<Page[]>(`/templates?Slug=${slug}`);
  return data;
};

// Transfrom path into array of strings for getStaticPaths
// e.g. "/nested/route" => ["nested", "route"]
export const getSlugs = async () => {
  const pages = await getAllPages();
  return pages.map((p) => ({
    params: { slug: p.Path?.split("/").slice(1) },
  }));
};
