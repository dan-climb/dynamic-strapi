import "@fontsource/dejavu-mono";
import { AppProps } from "next/app";
import Head from "next/head";
import React from "react";
import styled, { createGlobalStyle } from "styled-components";

export default function App({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Dynamic Strapi</title>
      </Head>
      <div>
        <Global />
        <TopBar />
        <Main>
          <Component {...pageProps} />
        </Main>
      </div>
    </>
  );
}

const Global = createGlobalStyle`
    body {
        font-family: "DejaVu Mono", Arial, Helvetica, sans-serif;
        -webkit-font-smoothing: antialiased;
        -moz-osx-font-smoothing: grayscale;
        color: #3B444A;
        margin: 0;
    }
`;

const TopBar = styled.div`
  width: 100%;
  height: 100px;
  background-color: coral;
`;

const Main = styled.main`
  padding: 15px;
`;
